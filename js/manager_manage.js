/* Java Script */
var xhr = null;

function getXMLHttpRequest() {
    if (window.ActiveXObject) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");//IE 상위 버젼
        } catch (e1) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");//IE 하위 버젼
            } catch (e2) {
                return null;
            }
        }
    } else if (window.XMLHttpRequest) {
        return new XMLHttpRequest();//IE 이외의 브라우저(FireFox 등)
    } else {
        return null;
    }
}// XMLHttpRequest 객체 얻기

function RespViewAgent() {
    if (xhr.readyState == 4) {//완료
        if (xhr.status == 200) {//오류없이 OK
            var str = xhr.responseText;//서버에서 보낸 내용 받기
            document.getElementById("ajax_modify_manager").innerHTML = str;//보여주기
        } else {
            alert("Fail : " + xhr.status);
        }
    }
}// 응답

function ReqViewAgent(num){
	URL='ajax/manager_modify.php';
	param = "agentNum=" + encodeURI(num);
	xhr = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr.open("POST", URL, true);//연결
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr.setRequestHeader("Content-length", param.length);
	xhr.setRequestHeader("Connection", "close");
    xhr.onreadystatechange = RespViewAgent;//콜백 함수  등록
    xhr.send(param);//전송
}
