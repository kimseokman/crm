/* java script */
var xhr = null;

function getXMLHttpRequest() {
    if (window.ActiveXObject) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");//IE 상위 버젼
        } catch (e1) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");//IE 하위 버젼
            } catch (e2) {
                return null;
            }
        }
    } else if (window.XMLHttpRequest) {
        return new XMLHttpRequest();//IE 이외의 브라우저(FireFox 등)
    } else {
        return null;
    }
}// XMLHttpRequest 객체 얻기

function ReqAutoInput(input_case){	//자동 입력 기능 요청
	URL='ajax/auto_input.php';
	param = "";

	if(input_case == "corp_n"){
		param = "corp_n=" + encodeURI(document.getElementById('corp_n').value);
	}else if(input_case == "recell"){
		param = "recell=" + encodeURI(document.getElementById('recell').value);
	}
	
	xhr5 = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr5.open("POST", URL, true);//연결
	xhr5.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr5.setRequestHeader("Content-length", param.length);
	xhr5.setRequestHeader("Connection", "close");
    xhr5.onreadystatechange = RespAutoInput;//콜백 함수  등록
    xhr5.send(param);//전송
}

function RespAutoInput(){	//자동 입력 기능 반응
	if (xhr5.readyState == 4) {//완료
        if (xhr5.status == 200) {//오류없이 OK
            var str = xhr5.responseText;//서버에서 보낸 내용 받기
            document.getElementById("auto_input").innerHTML = str;//보여주기		
        } else {
            alert("Fail : " + xhr5.status);
        }
    }
}

function ReqAutoAgentSum(){	//상담원 수 자동 합계 - DB에는 저장 안됨
	URL='ajax/auto_agent_sum.php';
	param = "PC_cnt=" + encodeURI(document.getElementById('PC_cnt').value) + "&M_cnt=" + encodeURI(document.getElementById('M_cnt').value);
	xhr4 = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr4.open("POST", URL, true);//연결
	xhr4.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr4.setRequestHeader("Content-length", param.length);
	xhr4.setRequestHeader("Connection", "close");
    xhr4.onreadystatechange = RespAutoAgentSum;//콜백 함수  등록
    xhr4.send(param);//전송
}

function RespAutoAgentSum(){	//상담원 수 자동 합계
	if (xhr4.readyState == 4) {//완료
        if (xhr4.status == 200) {//오류없이 OK
            var str = xhr4.responseText;//서버에서 보낸 내용 받기
            document.getElementById("agent_cnt").innerHTML = str;//보여주기		
        } else {
            alert("Fail : " + xhr4.status);
        }
    }
}

function ReqUsingProductEtc(){	//UsingProductEtc ajax 요청
	URL='ajax/using_product_etc.php';
	param = "etc=" + encodeURI(document.getElementById('etc').checked);
	xhr3 = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr3.open("POST", URL, true);//연결
	xhr3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr3.setRequestHeader("Content-length", param.length);
	xhr3.setRequestHeader("Connection", "close");
    xhr3.onreadystatechange = RespUsingProductEtc;//콜백 함수  등록
    xhr3.send(param);//전송
}

function RespUsingProductEtc(){	//UsingProductEtc ajax 반응
	if (xhr3.readyState == 4) {//완료
        if (xhr3.status == 200) {//오류없이 OK
            var str = xhr3.responseText;//서버에서 보낸 내용 받기
            document.getElementById("using_pord_etc").innerHTML = str;//보여주기		
        } else {
            alert("Fail : " + xhr3.status);
        }
    }
}

function ReqStopCustomEtc(c_case, c_case_2, cancel_etc){	//StopCustomEtc ajax 요청
	URL='ajax/stop_custom_etc.php';
	param = "";
	/**
	 * ajax c_case 값을 false 로 주면 
	 * 강제로 하위 ajax 다 숨기고
	 * param 값 빈값으로 넘김
	 */
	if(c_case == 'false'){	//false 일때 강제 hide
		param = "c_case=" + encodeURI(c_case) + 
			"&c_case_2=" + encodeURI(c_case_2) +
			"&cancel_etc=" + encodeURI(cancel_etc);
	/**
	 * ajax c_case 값을 ture 로 주면 
	 * 강제로 하위 ajax 다 보이고
	 * 기억하고 있던 값(db)으로
	 * param 값 넘김
	 */
	}else if(c_case == 'true'){	//true 일때 강제 show
		param = "c_case=" + encodeURI(c_case) + 
			"&c_case_2=" + encodeURI(c_case_2) +
			"&cancel_etc=" + encodeURI(cancel_etc);
	/**
	 * ajax c_case 값이 null이거나 ''일 경우
	 * 상황에 따른 ajax 반응 및 param 값 넘김
	  * 단 etc 값은 상황에 맞게 프로그래머가 빈값으로 넘기든 기억하고 있는 값으로 넘기든 해야함
	 */
	}else{
		param = "c_case=" + encodeURI(document.getElementById('c_case').checked) + 
			"&c_case_2=" + encodeURI(document.getElementById('c_case_2').value) +
			"&cancel_etc=" + encodeURI(cancel_etc);
	}
	
	xhr2 = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr2.open("POST", URL, true);//연결
	xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr2.setRequestHeader("Content-length", param.length);
	xhr2.setRequestHeader("Connection", "close");
    xhr2.onreadystatechange = RespStopCustomEtc;//콜백 함수  등록
    xhr2.send(param);//전송
}

function RespStopCustomEtc(){	//StopCustomEtc ajax 반응
	if (xhr2.readyState == 4) {//완료
        if (xhr2.status == 200) {//오류없이 OK
            var str = xhr2.responseText;//서버에서 보낸 내용 받기
            document.getElementById("stop_custom_etc").innerHTML = str;//보여주기		
        } else {
            alert("Fail : " + xhr2.status);
        }
    }
}

function ReqStopCustom(){	//StopCustom ajax 요청
	URL='ajax/stop_custom_select.php';
	param = "c_case=" + encodeURI(document.getElementById('c_case').checked);
	xhr = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr.open("POST", URL, true);//연결
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr.setRequestHeader("Content-length", param.length);
	xhr.setRequestHeader("Connection", "close");
    xhr.onreadystatechange = RespStopCustom;//콜백 함수  등록
    xhr.send(param);//전송
}

function RespStopCustom(){	//StopCustom ajax 반응
	if (xhr.readyState == 4) {//완료
        if (xhr.status == 200) {//오류없이 OK
            var str = xhr.responseText;//서버에서 보낸 내용 받기
            document.getElementById("stop_custom").innerHTML = str;//보여주기	

			$("#ci_cancel_date").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});

        } else {
            alert("Fail : " + xhr.status);
        }
    }
}


function price_divide(this_price){
	var input_price = "";
	var result_price = "";

	if(this_price == "price"){
		input_price = document.getElementById("price").value;//금액 String 변환
	}else if(this_price == "unit_price"){
		input_price = document.getElementById("unit_price").value;//금액 String 변환
	}

	input_price = input_price.replace(/,/g,'');	//다시 들어온 값을 위해서 콤마 걸러줌

	for(i=1; i<=input_price.length; i++){
		if(i > 1 && (i%3)==1)
			result_price = input_price.charAt(input_price.length - i) + "," + result_price;
		else
			result_price = input_price.charAt(input_price.length - i) + result_price;    
	}

	if(this_price == "price"){
		document.getElementById("price").value = result_price;
	}else if(this_price == "unit_price"){
		document.getElementById("unit_price").value = result_price;
	}
}

/* jquery */
$(function(){
	$("#first_intro").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$("#star_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$("#exp_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});
});
