/* java script */
var xhr = null;

function getXMLHttpRequest() {
    if (window.ActiveXObject) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");//IE 상위 버젼
        } catch (e1) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");//IE 하위 버젼
            } catch (e2) {
                return null;
            }
        }
    } else if (window.XMLHttpRequest) {
        return new XMLHttpRequest();//IE 이외의 브라우저(FireFox 등)
    } else {
        return null;
    }
}// XMLHttpRequest 객체 얻기

function ReqKeyWord2(){	//keyword2 ajax 요청
	URL='ajax/keyword2.php';
	param = "s_keyword=" + encodeURI(document.getElementById('s_keyword').value);
	xhr = getXMLHttpRequest();//XMLHttpRequest 객체 얻기
	xhr.open("POST", URL, true);//연결
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr.setRequestHeader("Content-length", param.length);
	xhr.setRequestHeader("Connection", "close");
    xhr.onreadystatechange = RespKeyWord2;//콜백 함수  등록
    xhr.send(param);//전송
}

function RespKeyWord2(){	//keyword2 ajax 반응
	if (xhr.readyState == 4) {//완료
        if (xhr.status == 200) {//오류없이 OK
            var str = xhr.responseText;//서버에서 보낸 내용 받기
            document.getElementById("keyword2").innerHTML = str;//보여주기	
			
			$("#keyword1_1_s_date").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});
			$("#keyword1_1_e_date").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});
			$("#keyword1_1_text").keypress(function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
					search_custom();
				}
			});
        } else {
            alert("Fail : " + xhr.status);
        }
    }
}

function customNumSubmit(ci_num){
	document.getElementById('customNum').value = ci_num;
	document.getElementById('modifyCustomNum').submit();
}

function Excel_Save(){
	document.getElementById('excelForm').submit();
}

function search_custom(){
	var search_form = document.getElementById('search_form');
	var key1 = document.getElementById('s_keyword').value;
	var key1_1_text = '';
	var key1_1_radio = '1';
	var key1_1_select = '';
	var key1_1_s_date = '';
	var key1_1_e_date = '';
	
	if(document.getElementById('keyword1_1_text') != null){
		key1_1_text = document.getElementById('keyword1_1_text').value;
	}
	
	if(document.getElementById('keyword1_1_select') != null){
		key1_1_select = document.getElementById('keyword1_1_select').value;
	}
	
	if((document.getElementById('keyword1_1_s_date') != null) && (document.getElementById('keyword1_1_e_date') != null)){
		key1_1_s_date = document.getElementById('keyword1_1_s_date').value;
		key1_1_e_date = document.getElementById('keyword1_1_e_date').value;
	}
	
	if(document.getElementById('keyword1_1_radio_in') != null){
		if(document.getElementById('keyword1_1_radio_in').checked)			key1_1_radio = 1;	
		else if(document.getElementById('keyword1_1_radio_out').checked)	key1_1_radio = 0;
	}
	
	search_form.bef_keyword1.value = key1;
	search_form.bef_keyword1_1_text.value = key1_1_text;
	search_form.bef_keyword1_1_select.value = key1_1_select;
	search_form.bef_keyword1_1_radio.value = key1_1_radio;
	search_form.bef_keyword1_1_date_s.value = key1_1_s_date;
	search_form.bef_keyword1_1_date_e.value = key1_1_e_date;
	
	search_form.search_flag.value = 'Y';
	search_form.submit();
}

 function page_submit(click_page){
	var page_form = document.getElementById('page_form');
	page_form.pg.value = click_page;
	page_form.submit();
 }

/* jquery */
$(function(){
	$("#service_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$("#service_exp_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$( "#company_name" ).on('click',function(){
		ReqKeyWord2();
	});

	$( "#company_name" ).trigger( "click" );
});