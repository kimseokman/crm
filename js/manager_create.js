/* Java Script */
function checkPW(obj){
	var inputPW = document.getElementById("inputPW");
	var chkIcon = document.getElementById("checkPWIcon");

	if(obj.value.split(" ").join("").length == 0){
		inputPW.setAttribute('class','form-group has-feedback has-error');
		chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-remove');
		return;
	}else{
		inputPW.setAttribute('class','form-group has-feedback has-success');
		chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-ok');
	}	
}

function checkID(obj){
	var strResponseURL = '_check_id.php';
	var inputID = document.getElementById("inputID");
	var chkIcon = document.getElementById("checkIcon");
	var chk = document.getElementById("checkMsg");
	var chkVal = document.getElementById("idCheck");

	if(obj.value.split(" ").join("").length == 0){
		inputID.setAttribute('class','form-group has-feedback has-warning');
		chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-warning-sign');
		chk.innerHTML = "아이디를 입력해 주세요.";
		chk.style.color = "#f0ad4e";
		chkVal.value = "0";
		return;
	}
	if(obj.value.length > 12){
		inputID.setAttribute('class','form-group has-feedback has-warning');
		chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-warning-sign');
		chk.innerHTML = "12글자를 넘지않게 입력하세요.";
		chk.style.color = "#f0ad4e";
		chkVal.value = "0";
		return;
	}
	for(i=0;i<obj.value.length;i++) {
		var c = obj.value.charAt(i);
		if((c < '0' || c > '9')&&(c < 'a' || c > 'z')&&(c < 'A' || c > 'Z')&&((c != '-')&&(c != '_'))){
			inputID.setAttribute('class','form-group has-feedback has-warning');
			chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-warning-sign');
			chk.innerHTML = "아이디는 영어, 숫자, '-', '_', '.'만 가능 합니다.";
			chk.style.color = "#f0ad4e";
			chkVal.value = "0";
			return;
		}
	}
	//alert("1");
	var httpObj = new Ajax.Request(
		strResponseURL, { 
			method:'post', 
			parameters:{
				checkVal:obj.value
			},
		onSuccess:function (responseHttpObj) {
			var tmp = responseHttpObj.responseText;
			//alert(tmp);
			var inputID = document.getElementById("inputID");
			var chkIcon = document.getElementById("checkIcon");
			var chk = document.getElementById("checkMsg");
			var chkVal = document.getElementById("idCheck");
				
			if(tmp.indexOf("0") >= 0){ // 매니저 중복
				//alert(3);
				inputID.setAttribute('class','form-group has-feedback has-error');
				chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-remove');
				chk.innerHTML = "중복되는 아이디입니다.";
				chk.style.color = "red";
				chkVal.value = "0";
			}else if(tmp.indexOf("1")>=0){ // 관리자 중복
				//alert(5);
				inputID.setAttribute('class','form-group has-feedback has-error');
				chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-remove');
				chk.innerHTML = "중복되는 아이디입니다.";
				chk.style.color = "red";
				chkVal.value = "0";
			}else{
				//alert(1);
				inputID.setAttribute('class','form-group has-feedback has-success');
				chkIcon.setAttribute('class','glyphicon form-control-feedback glyphicon-ok');
				chk.innerHTML = "사용할 수 있는 아이디입니다.";
				chk.style.color = "green";
				chkVal.value = "1";
			}
		}
	});
	//alert(document.getElementById("idCheck").value);
}