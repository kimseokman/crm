<html>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8"/>
<?
$msgTag = "";
$msgLevel = "";
$msgIcon = "";

//기본 경로
$target_path = "uploads/";
//회사 이름
$adID_path = $_POST['adID']."/";
//거래 목록
$history_path = $_POST['bhNum']."/";

$target_path = $target_path.$adID_path.$history_path;
// 경로 생성
if(!is_dir($target_path)){
	umask(0);
	@mkdir($target_path, 0777, true);	//에러코드 없애기 [보안]
}

//echo "최종경로 : ".$target_path."<br />";

//최종 경로 파일 이동
//$input_size = count($_FILES['files']['size']);
for($i=0; $i<count($_FILES['files']['size']); $i++){
	
	//echo "임시폴더 : ".$_FILES["files"]["tmp_name"][$i]."<br/> 파일명 : ".basename($_FILES['files']['name'][$i])."<br/> 최종경로 : ".$target_path.basename($_FILES['files']['name'][$i])."<br/>";
	
	if($_FILES['files']["error"][$i] > 0){	//에러 검출
		if($_FILES["files"]["error"][$i] == 1){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 용량이 너무 큽니다.(php)";
		}else if($_FILES["files"]["error"][$i] == 2){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 용량이 너무 큽니다.(html)";
		}else if($_FILES["files"]["error"][$i] == 3){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 파일이 일부분만 전송되었습니다.";
		}else if($_FILES["files"]["error"][$i] == 4){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 파일이 전송되지 않았습니다.";
		}else if($_FILES["files"]["error"][$i] == 5){
			
		}else if($_FILES["files"]["error"][$i] == 6){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 임시 폴더가 없습니다.";
		}else if($_FILES["files"]["error"][$i] == 7){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 디스크에 파일 쓰기를 실패했습니다.";
		}else if($_FILES["files"]["error"][$i] == 8){
			$msgTag = "ERROR CODE [".$_FILES["files"]["error"][$i]."] 확장에 의해 파일 업로드가 중지되었습니다.";
		}else{
			$msgTag = "ERROR CODE : ".$_FILES["files"]["error"][$i];
		}
		
		$msgLevel = "danger";
		$msgIcon = "ban-circle";
	}else{
		$filename = iconv("UTF-8","EUC-KR",$_FILES['files']['name'][$i]);//한글파일명
		$filename = str_replace(' ','_',$filename);	//공백 치환

		if(is_file($target_path.$filename)){//파일명 중복
			$msgTag = "이미 존재하는 파일명 입니다.";
			$msgLevel = "danger";
			$msgIcon = "ban-circle";
			//$err_msg = "이미 존재하는 파일명 입니다.";
			//$err_msg = iconv("UTF-8","EUC-KR",$err_msg);//에러 메시지 한글
			//echo "<script>alert('$err_msg'); history.go(-1);</script>";
		}else{
			move_uploaded_file($_FILES["files"]["tmp_name"][$i], $target_path.$filename);
			$msgTag = "파일이 업로드 되었습니다.";
			//$msgTag = iconv("UTF-8","EUC-KR",$msgTag);//메시지 한글
			$msgLevel = "success";
			$msgIcon = "ok-circle";
		}
	}
}

?>
<body>
<form method='post' id='tempForm' action='custom_modify.html'>
	<input type='hidden' name='customNum' value='<? echo $_POST['cusNum']; ?>' />
	<input type="hidden" name="alertMsg" value='<?echo $msgTag;?>' />	
	<input type="hidden" name="alertLv" value='<?echo $msgLevel;?>' />	
	<input type="hidden" name="alertIcon" value='<?echo $msgIcon;?>' />
</form>
</body>
</html>
<?
//echo "넘어오는 값:".$_POST['cusNum'];
echo "<script>document.getElementById('tempForm').submit();</script>";
?>