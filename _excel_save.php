<?
	@session_start();

	header( "Content-type: application/vnd.ms-excel" );  
	header( "Content-Disposition: attachment; filename=report.xls"); 
	header( "Content-Description: PHP4 Generated Data" ); 

	include dirname(__FILE__)."/class/CAgent.php";
	
	if($_POST['excel_search_flag'] == '' || $_POST['excel_search_flag'] == null){
		$_POST['excel_search_flag'] = 'N';
	}
?>

<html>
<head>
<title></title>
</head>
<body>
<div class="custom_search_content" id="custom_search_content">
	<table cellpadding="0" cellspacing="0" border="0" class="display" id="viewCustom" summary="만료일7일 이내의 고객 정보를 보여주는 게시판으로서 만료일, 회사명, 기간, 수량, 금액, 신청담당자, 연락처, 참고사항, 사용량 으로 이루어져있다.">
		<thead>
			<tr>
				<th scope="col">만료일</th>
				<th scope="col">회사명</th>
				<th scope="col">상담원ID</th>
				<th scope="col">기간</th>
				<th scope="col">수량</th>
				<th scope="col">금액</th>
				<th scope="col">신청담당자</th>
				<th scope="col">연락처</th>
				<th scope="col">참고사항</th>
				<th scope="col">사용량</th>
			</tr>
		</thead>
		<tbody>
<?
	$cus_han = new CAgent();
	$ci_sub_han = new CAgent();
	$bh_sub_han = new CAgent();
	$pi_sub_han = new CAgent();

	if($_POST['excel_search_flag'] == 'N'){
		//default sql 지정
		$sql = "SELECT DISTINCT business_history.ci_num 
				FROM business_history, custom_info 
				WHERE to_days(business_history.bh_exp_date) - to_days(curdate())<= '7' 
				AND business_history.bh_renew != '1' 
				AND custom_info.ci_num = business_history.ci_num";
	}else if($_POST['excel_search_flag'] == 'Y'){
		/**
		 * Key word 1에 대한 쿼리 설정
		 */
		$keyword1 = "";

		/**
		 * table에 따라 쿼리를 적용한다.
		 */
		$table_tag = 0;	//0: 전체  1: ci 테이블  2. bh 테이블

		switch($_POST['excel_keyword1']){
			case 0:		$keyword1 = " ";											$table_tag = 0;	break;	//선택 X __ 전범위 검색
			case 1:		$keyword1 = " custom_info.ci_corp_name ";					$table_tag = 1;	break;	//회사명
			case 2:		$keyword1 = " custom_info.ci_case ";						$table_tag = 1;	break;	//고객구분
			case 3:		$keyword1 = " custom_info.ci_using_prod ";					$table_tag = 1;	break;	//기존 사용 제품
			case 4:		$keyword1 = " custom_info.ci_info ";						$table_tag = 1;	break;	//고객 정보
			case 5:		$keyword1 = " custom_info.ci_bsns ";						$table_tag = 1;	break;	//영업 성격
			case 6:		$keyword1 = " custom_info.ci_serv_name ";					$table_tag = 1;	break;	//서비스명
			case 7:		$keyword1 = " custom_info.ci_receller ";					$table_tag = 1;	break;	//리셀러
			case 8:		$keyword1 = " custom_info.ci_tax_bill_publish ";			$table_tag = 1;	break;	//세계 발행처
			case 9:		$keyword1 = " custom_info.ci_home_page ";					$table_tag = 1;	break;	//홈페이지
			case 10:	$keyword1 = " custom_info.ci_ad_id ";						$table_tag = 1;	break;	//admin
			case 11:	$keyword1 = " business_history.bh_charge_name ";			$table_tag = 2;	break;	//영업자
			case 12:	$keyword1 = " custom_info.ci_corp_regi_num ";				$table_tag = 1;	break;	//사업등록번호
			case 13:	$keyword1 = " to_days(custom_info.ci_intro_date) ";			$table_tag = 1;	break;	//도입일
			case 14:	$keyword1 = " to_days(business_history.bh_exp_date) ";		$table_tag = 2;	break;	//만료일
			case 15:	$keyword1 = " custom_info.ci_manage_num ";					$table_tag = 1;	break;	//관리번호
			case 16:	$keyword1 = " business_history.bh_prod_case_m ";			$table_tag = 2;	break;	//M사용고객
			case 17:	$keyword1 = " custom_info.ci_bsns_type ";					$table_tag = 1;	break;	//업종
			case 18:	$keyword1 = " business_history.bh_ag_id ";					$table_tag = 2;	break;	//상담원ID
			case 19:	$keyword1 = " to_days(custom_info.ci_case_cancel_date) ";	$table_tag = 1;	break;	//해지일
			default:	//default 일리 없지만..
		}

		/**
		 * Key word 1에 대한 Key word 1-1 쿼리 설정
		 */
		 
		$keyword2 = "";

		 //post 값 필터
		if($_POST['excel_keyword1_1_text'] == null) $_POST['excel_keyword1_1_text'] = "";
		if($_POST['excel_keyword1_1_select'] == null) $_POST['excel_keyword1_1_select'] = "0";
		if($_POST['excel_keyword1_1_radio'] == null) $_POST['excel_keyword1_1_radio'] = '1';
		if($_POST['excel_keyword1_1_date_s'] == null) $_POST['excel_keyword1_1_date_s'] = "";
		if($_POST['excel_keyword1_1_date_e'] == null) $_POST['excel_keyword1_1_date_e'] = "";
		
		switch($_POST['s_keyword']){
			case 0:		$keyword2 = " ";																							break;	//선택 X __ 전범위 검색
			case 1:		$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//회사명
			case 2:		$keyword2 = " = '".$_POST['excel_keyword1_1_select']."'";													break;	//고객구분
			case 3:		$keyword2 = " & ".$_POST['excel_keyword1_1_select']." > 0 ";												break;	//기존 사용 제품
			case 4:		$keyword2 = " = '".$_POST['excel_keyword1_1_radio']."'";													break;	//고객 정보
			case 5:		$keyword2 = " = '".$_POST['excel_keyword1_1_radio']."'";													break;	//영업 성격
			case 6:		$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//서비스명
			case 7:		$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//리셀러
			case 8:		$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//세계 발행처
			case 9:		$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//홈페이지
			case 10:	$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//admin
			case 11:	$keyword2 = " like '%".$_POST['excel_keyword1_1_select']."%'";												break;	//영업자
			case 12:	$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//사업등록번호
			case 13:	$keyword2 = " >= to_days('".$_POST['excel_keyword1_1_s_date']."') 
									AND to_days(custom_info.ci_intro_date) <= to_days('".$_POST['excel_keyword1_1_e_date']."')";	break;	//도입일
			case 14:	$keyword2 = " >= to_days('".$_POST['excel_keyword1_1_s_date']."') 
									AND to_days(business_history.bh_exp_date) <= to_days('".$_POST['excel_keyword1_1_e_date']."')";	break;	//만료일
			case 15:	$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//관리번호
			case 16:	$keyword2 = " >= '0'";																						break;	//M사용고객
			case 17:	$keyword2 = " = '".$_POST['excel_keyword1_1_select']."'";													break;	//업종
			case 18:	$keyword2 = " like '%".$_POST['excel_keyword1_1_text']."%'";												break;	//상담원ID
			case 19:	$keyword2 = " >= to_days('".$_POST['excel_keyword1_1_s_date']."') 
									AND to_days(custom_info.ci_case_cancel_date) <= to_days('".$_POST['excel_keyword1_1_e_date']."')";	break;	//해지일
			default: //default 일리 없지만..
		}

		/**
		 * 쿼리 조합
		 * - 최종 쿼리
		 */

		 $sql = "";
		
		if($table_tag == 0){
			$sql_head = "SELECT ci_num FROM custom_info ";
		}else if($table_tag == 1){
			$sql_head = "SELECT ci_num FROM custom_info WHERE";
		}else if($table_tag == 2){
			$sql_head = "SELECT DISTINCT business_history.ci_num FROM custom_info, business_history WHERE custom_info.ci_num = business_history.ci_num AND ";
		}
		
		$sql_body = $keyword1.$keyword2;
		

		$sql = $sql_head . $sql_body;
	}

	$condi_info = $cus_han->cmysql->QueryFetch($sql);
	
	/**
	 *	해당 조건에 대해 한번 검색 하여 Custom Info 테이블의 primary key 찾아낸 후
	 */

	do{
		$ci_num = $condi_info->ci_num;

		$main_sql = "SELECT * FROM custom_info WHERE ci_num = '".$ci_num."'";
		$cus_info = $ci_sub_han->cmysql->QueryFetch($main_sql);
		
		do{

			$main_sql2 = "SELECT * FROM business_history WHERE ci_num = '".$cus_info->ci_num."' ORDER BY business_history.bh_exp_date DESC LIMIT 0,1";
			$bsns_his = $bh_sub_han->cmysql->QueryFetch($main_sql2);

			$main_sql3 = "SELECT * FROM person_info WHERE ci_num = '".$cus_info->ci_num."' AND pi_type = '0' ";
			$per_info = $pi_sub_han->cmysql->QueryFetch($main_sql3);
?>
					<tr>
						<td>
<?
			if($bsns_his->bh_exp_date){
				if($bsns_his->bh_exp_date != "0000-00-00")
					echo $bsns_his->bh_exp_date;
				else
					echo "-";
			}else {
				echo "-";
			}
?>
						</td>
						<td><?=$cus_info->ci_corp_name ?></td>
						<td>
<? 
			if($bsns_his->bh_ag_id){
				echo $bsns_his->bh_ag_id; 
			} else {
				echo "-"; 
			}
?>
						</td>
						<td>
<?
			if($bsns_his->bh_using_term){//값이 있다면
				echo $bsns_his->bh_using_term."개월";
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>
<?
			if($bsns_his->bh_prod_cnt){//값이 있다면
				echo $bsns_his->bh_prod_cnt;
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>
<?
			if($bsns_his->bh_sum_price){//값이 있다면
				echo $bsns_his->bh_sum_price;
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>
<?
			if($per_info->pi_name){//값이 있다면
				echo $per_info->pi_name;
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>
<?
			if($per_info->pi_tel1){//값이 있다면
				echo $per_info->pi_tel1;
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>
<?
			if($bsns_his->bh_refer){//값이 있다면
				echo $bsns_his->bh_refer;
			}else{	//값이 없다면
				echo "-";
			}
?>
						</td>
						<td>기존 DB에서 읽어와야함</td>
					</tr>
<?
		} while (($cus_info = $ci_sub_han->cmysql->NextFetch()) != NULL);
	} while (($condi_info = $cus_han->cmysql->NextFetch()) != NULL);
?>
		</tbody>
	</table>
</div>
<!-- //custom_search_content -->
</body>
</html>
