<?
	@session_start();
?>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8"/>
<?
	include dirname(__FILE__)."/class/CAgent.php";

	$ag_han = new CAgent();
	$cus_han = new CAgent();
	$msgTag = "";
	$msgLevel = "";
	$msgIcon = "";
	$adminIDexist = false;

	if($_POST['adminID'] != ""){
		$search_adminID_sql = "SELECT * FROM custom_info WHERE custom_info.ci_ad_id = '".$_POST['adminID']."'";
		$cus_info = $cus_han->cmysql->QueryFetch($search_adminID_sql);
		if($cus_info->ci_num != ""){	//adminID 이미 존재(중복)
			$adminIDexist = true;
		}
	}
	
	//form validation
	if($_POST['corp_n'] == ""){		//회사명
		$msgTag = "회사명을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['srvc_n'] == ""){	//서비스명
		$msgTag = "서비스명을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['regi_num'] == ""){	//사업자 등록번호
		$msgTag = "사업자 등록번호를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['mng_num'] == ""){	//관리번호
		$msgTag = "관리번호를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['adminID'] == ""){	//관리자 아이디(필수)
		$msgTag = "ADMIN ID를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($adminIDexist){			//관리자 아이디(중복)
		$msgTag = "이미 존재하는 ADMIN ID 입니다.";
		$msgLevel = "danger";
		$msgIcon = "ban-circle";
	}else if($_POST['connect_url'] == ""){	//접속 URL
		$msgTag = "접속 URL을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if(($_POST['PC_cnt'] == "") && ($_POST['M_cnt'] == "")){	//상담원 ID 수
		$msgTag = "AGENT ID 수를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['a_n'] == ""){	//신청자 이름
		$msgTag = "신청 담당자 이름을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['a_e'] == ""){	//멜 주소
		$msgTag = "신청 담당자 e mail 주소를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else{
		
		/**
		 * 데이터 setting
		 */

		/**
		 * 고객 구분 setting
		 * radio
		 */

		 //고객 구분 == 해지고객
		if($_POST['c_case'] == "2"){
			$case_date = $_POST['ci_cancel_date'];		//해지일 입력
			$case_reason = $_POST['c_case_2'];		//해지 사유 종류
		}else{
			$case_date = "00000000";			//아닐때 빈칸
			$case_reason = "0";
		}

		//해지 사유 == 기타
		if($_POST['c_case_2'] == 15){
			$case_reason_etc = $_POST['stop_reason_etc'];
		}else{
			$case_reason_etc = "0";
		}

		if($_POST['first_intro'] == null){
			$_POST['first_intro'] = '00000000';
		}
		

		/**
		 * 기존 사용 제품 setting
		 * checkbox
		 */
		$using_prod = 0;
		if($_POST['remotecall']== "on")	$using_prod += 1;	//리모트콜
		if($_POST['aranta']== "on")		$using_prod += 2;	//아란타
		if($_POST['easyhelp']== "on")	$using_prod += 4;	//이지헬프
		if($_POST['anyhelp']== "on")	$using_prod += 8;	//애니헬프
		if($_POST['idoctor']== "on")	$using_prod += 16;	//아이닥터
		if($_POST['helpcom']== "on")	$using_prod += 32;	//헬프컴
		if($_POST['teamviewer']== "on")	$using_prod += 64;	//팀뷰어
		if($_POST['newintro']== "on")	$using_prod += 128;	//신규도입
		if($_POST['etc']== "on"){							//기타
			$using_prod += 256;
			$etc_txt = $_POST['etc_txt'];					//기타 선택시 기타 제품명 입력
		}else{
			$etc_txt = "";									//기타 입력 없으면 빈칸
		}
		
		/**
		 * 고객 정보 setting
		 * radio
		 */
		if($_POST['c_info'] == "in")	$c_info=1;
		else							$c_info=0;

		/**
		 * 영업 성격 setting
		 * radio
		 */
		if($_POST['b_char'] == "in")	$b_char=1;
		else							$b_char=0;

		/**
		 * 예비서버 제공 setting
		 * checkbox
		 */
		$pre_server = 0;
		if($_POST['pre_server_28']== "on")	$pre_server += 1;		//28번 서버
		if($_POST['pre_server_35']== "on")	$pre_server += 2;	//35번 서버
		if($_POST['pre_server_55']== "on")	$pre_server += 4;	//55번 서버
		if($_POST['pre_server_47']== "on")	$pre_server += 8;	//47번 서버

		/**
		 * custom_info table data insert
		 * database : crm
		 */
		$ci_sql = 
		"INSERT INTO Custom_info
		(ci_case, ci_case_cancel_date, ci_case_cancel_case, ci_case_cancel_case_etc, 
		ci_using_prod, ci_etc_prod, 
		ci_info, ci_bsns, ci_pre_server,
		ci_corp_name, ci_serv_name, ci_receller, ci_tax_bill_publish, ci_home_page, ci_intro_date, ci_corp_regi_num, ci_manage_num, ci_ad_id, 
		ci_pc_ag_cnt, ci_mb_ag_cnt, 
		ci_connect_url, ci_bsns_type, ci_regi_send_addr, ci_report, ci_document, ci_pay_content, ci_note) VALUES ('".$_POST['c_case']."', '".$case_date."', '".$case_reason."', '".$case_reason_etc."', 
		'".$using_prod."', '".$etc_txt."', 
		'".$c_info."', '".$b_char."', '".$pre_server ."',
		'".$_POST['corp_n']."', '".$_POST['srvc_n']."', '".$_POST['recell']."', '".$_POST['tax_bill']."', '".$_POST['home_p']."', '".$_POST['first_intro']."', '".$_POST['regi_num']."', '".$_POST['mng_num']."', '".$_POST['adminID']."',
		'".$_POST['PC_cnt']."', '".$_POST['M_cnt']."', 
		'".$_POST['connect_url']."', '".$_POST['b_type']."', '".$_POST['regi_addr']."', '".$_POST['report']."', '".$_POST['docu']."', '".$_POST['pay_content']."', '".$_POST['c_note']."')";	
	
		if($sql_c_re = $ag_han->CreateCustom($ci_sql)=="OK"){	//custom_info 저장 후에
			//한번 찾고
			$sql = "select * from Custom_info where ci_ad_id='".$_POST['adminID']."'";

			$cus_han = $ag_han->GetCustom($sql);
			$cus_han->ci_num;

			/**
			 * person_info table data insert
			 * database : crm
			 */
			//신청 담당자 저장
			$pi_a_sql = "INSERT INTO Person_info
			(pi_name, pi_position, pi_depart, pi_tel1, pi_tel2, pi_email, pi_type, ci_num) VALUES 
			('".$_POST['a_n']."', '".$_POST['a_p']."', '".$_POST['a_d']."', '".$_POST['a_t1']."', '".$_POST['a_t2']."', '".$_POST['a_e']."', '0', '".$cus_han->ci_num."')";

			$sql_p_re = $ag_han->CreateCustom($pi_a_sql);

			//결제 담당자 저장
			$pi_p_sql = "INSERT INTO Person_info
			(pi_name, pi_position, pi_depart, pi_tel1, pi_tel2, pi_email, pi_type, ci_num) VALUES 
			('".$_POST['p_n']."', '".$_POST['p_p']."', '".$_POST['p_d']."', '".$_POST['p_t1']."', '".$_POST['p_t2']."', '".$_POST['p_e']."', '1', '".$cus_han->ci_num."')";

			$sql_p_re = $ag_han->CreateCustom($pi_p_sql);

			//영업 담당자 저장
			$pi_r_sql = "INSERT INTO Person_info
			(pi_name, pi_position, pi_depart, pi_tel1, pi_tel2, pi_email, pi_type, ci_num) VALUES 
			('".$_POST['r_n']."', '".$_POST['r_p']."', '".$_POST['r_d']."', '".$_POST['r_t1']."', '".$_POST['r_t2']."', '".$_POST['r_e']."', '2', '".$cus_han->ci_num."')";

			$sql_p_re = $ag_han->CreateCustom($pi_r_sql);
			
			/**
			 * business_history table data insert
			 * database : crm
			 */
		
			//Business_info data
			if($_POST['s_date'] == null)	$_POST['s_date'] = '00000000';
			if($_POST['e_date'] == null)	$_POST['e_date'] = '00000000';
			$renew = 0;	//갱신여부

			if(($_POST['agentID'] == null) || ($_POST['agentID'] == "")){
			}else{
				$bi_sql = "INSERT INTO Business_history
				(bh_ag_id, bh_prod_case_pc, bh_prod_case_m, 
				bh_star_date, bh_exp_date, 
				bh_prod_cnt, bh_using_term, 
				bh_sum_price, bh_unit_price, 
				bh_charge_name, 
				bh_info_name, bh_info_call, bh_info_e_mail,
				bh_refer, bh_contract, bh_renew, ci_num) VALUES 
				('".$_POST['agentID']."', '".$_POST['PnM_P']."', '".$_POST['PnM_M']."', 
				'".$_POST['s_date']."', '".$_POST['e_date']."', 
				'".$_POST['cnt']."', '".$_POST['term']."', 
				'".$_POST['price']."', '".$_POST['unit_price']."', 
				'".$_POST['person_name']."', 
				'".$_POST['person_n']."', '".$_POST['person_c']."', '".$_POST['person_e']."',
				'".$_POST['refer']."', '".$_POST['contract']."', '".$renew."', '".$cus_han->ci_num."')";

				$sql_b_re = $ag_han->CreateCustom($bi_sql);
			}

			$msgTag = "고객이 생성 되었습니다.";
			$msgLevel = "success";
			$msgIcon = "ok-circle";

		}//end of if($sql_c_re = $ag_han->CreateCustom($ci_sql)=="OK")
	}//end of case by success

	if($msgLevel != "success"){	//오류
?>
<form method="post" id="customAlertMsgForm" action="custom_regi.html">
<?
	}else{//정상
?>
<form method="post" id="customAlertMsgForm" action="custom_modify.html">
<?
	}
?>
	<input type="hidden" name="customNum" value='<? echo $cus_han->ci_num; ?>' />
	<input type="hidden" name="alertMsg" value='<?echo $msgTag;?>' />	
	<input type="hidden" name="alertLv" value='<?echo $msgLevel;?>' />
	<input type="hidden" name="alertIcon" value='<?echo $msgIcon;?>' />	
</form>
<?
	
	echo "<script>document.getElementById('customAlertMsgForm').submit();</script>";
?>