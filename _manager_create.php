<?
	@session_start();
?>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8"/>
<?
	include dirname(__FILE__)."/class/CAdmin.php";

	$admin_han = new CAdmin();
	$msgTag = "";
	$msgLevel = "";
	$msgIcon = "";

	$re_auth = 0;
	//권한값 2진법 전환
	if($_POST['auth_excel'] != "")
		$auth_s = 1;
	if($_POST['auth_modify'] != "")
		$auth_m = 2;
	if($_POST['auth_down'] != "")
		$auth_d = 4;
	$re_auth = $auth_s + $auth_m + $auth_d;

	//form data validation
	//case by id
	if($_POST['agentID'] == ""){
		$msgTag = "ID를 입력 해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['idCheck'] != 1){
		$msgTag = "이미 존재하는 ID 입니다.";
		$msgLevel = "danger";
		$msgIcon = "ban-circle";
	}
	//case by pass
	else if($_POST['agentPW'] == ""){
		$msgTag = "Password를 입력 해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['agentPW'] != $_POST['agentPW2']){
		$msgTag = "Password를 확인 해주세요.";
		$msgLevel = "danger";
		$msgIcon = "ban-circle";
	}
	//case by success
	else{
		$sql="INSERT INTO user_info(ui_id, ui_pw, ui_auth) VALUES ('".$_POST['agentID']."', password('".$_POST['agentPW']."'), ".$re_auth.")";

		$sql_re = $admin_han->CreateAgent($sql);
		if($sql_re == "OK"){
			$msgTag = "Manager가 성공적으로 생성되었습니다.";
			$msgLevel = "success";
			$msgIcon = "ok-circle";
		}	
	}

	if($msgLevel != "success"){	//오류
?>
<form method="post" id="agentAlertMsgForm" action="manager_create.html">
<?
	}else{//정상
?>
<form method="post" id="agentAlertMsgForm" action="manager_manage.html">
<?
	}
?>
	<input type="hidden" name="alertMsg" value='<?echo $msgTag;?>' />	
	<input type="hidden" name="alertLv" value='<?echo $msgLevel;?>' />	
	<input type="hidden" name="alertIcon" value='<?echo $msgIcon;?>' />	
</form>
<?
	echo "<script>document.getElementById('agentAlertMsgForm').submit();</script>";
?>