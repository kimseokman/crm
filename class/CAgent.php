<?
	include dirname(__FILE__)."/CMysql.php";

	class CAgent
	{
		var $cmysql;

		function CAgent()
		{
			$this->cmysql = new CMysql();
		}

		function CreateCustom($sql)
		{
			// id나 password 입력이 없을 경우 
			/*****************************************************/
			/* 2009.04.13 			     		     */
			/* mysql이 연결이 되지 않아도 홈페이지는 열려야 한다 */
			/*****************************************************/
			
			if($this->cmysql->connect == null)
				return "DB_ERROR";
			/*****************************************************/

			$this->cmysql->Query($sql);

			return "OK";
		}

		function GetCustom($sql)
		{
			/*****************************************************/
			
			if($this->cmysql->connect == null)
				return "DB_ERROR";
			/*****************************************************/

			$this->cmysql->QueryFetch($sql);

			return $this->cmysql->row;
		}

		//function GetSelectCustom()
	}
?>