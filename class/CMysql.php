<?

	//
	// Class: CMysql
	// 
	// Description: 
	//
	//	Mysql 연결이나 쿼리에 관한 역할을 한다.
	//
	
	class CMysql
	{
		var $connect = null;
		var $num;
		var $result;
		var $row;
		
		
		//
		// Method: CMysql
		//
		// Description: 
		//
		//	선언자로서 인스턴스가 만들어졌을 경우 연결을 만들고 DB를 선택한다.
		
		function CMysql() 
		{
			
			
			//$fp = fopen(dirname( __FILE__ )."\db_addr.txt" , "r");
			
			//if(!$fp)
			//{
			//	$current_dbaddr = "localhost";	
			//}
			//else
			//{
			//	$current_dbaddr = fread($fp , 16);
			//	fclose($fp);
				
			//}$current_dbaddr
			
			$this->connect = mysql_connect("localhost", "root", "1234");
				//or die("Cannot connect to MySQL Server : ".mysql_error());

			if($this->connect)
				mysql_select_db("crm", $this->connect);
				//or die("Cannot select DB! : ".mysql_error());

			//utf8로 문자세트 변경 //mysql 한글깨짐현상
			mysql_set_charset("utf-8", $this->connect);
			mysql_query("set names utf8");
				
				
			
		}
	
		//
		// Method: Close
		//
		// Description: 
		//
		//	Mysql 연결을 닫음으로써 핸들을 닫는다.
		//
		
		
		function Close()
		{
			mysql_close($this->connect) 
				or die("Cannot close MySQL! : " . mysql_error());
		}	
		
		//
		// Method: Query
		//
		// Description: 
		//
		//	Query만을 수행하는 함수
		//
		
		function Query($sql)
		{
			$this->result = mysql_query($sql, $this->connect) 
				//or Invalid query: " . $sql . " | " . mysql_error());	
				or die(" $sql <br> 잘못된 쿼리입니다. 관리자에게 문의하십시오."); 
					
		}
		
		//
		// Method: QueryNum
		//
		// Description: 
		//
		//	Query를 하고 라인수까지 리턴하는 함수
		//

		function QueryNum($sql)
		{
			$this->Query($sql);
			$this->num = mysql_num_rows($this->result);
		}
	
		//
		// Method: QueryFetch
		//
		// Description: 
		//
		//	Query에 라인수와 Fetch된 열까지 리턴하는 함수
		//
	
		function QueryFetch($sql)
		{
			$this->QueryNum($sql);
			if($this->num == 0) return NULL;
			else {
				$this->row = mysql_fetch_object($this->result);
				return $this->row;
			}
		}
	
		//
		// Method: NextFetch
		//
		// Description: 
		//
		//	다음 데이터를 Fetch한다.
		//
		
		function NextFetch()
		{
			if($this->row != NULL) {
				$this->row = mysql_fetch_object($this->result);
				return $this->row;
			}
			else return NULL;
		}
		
		//
		// Method: Encryption
		//
		// Description:
		//
		//	암호화된 문자열을 구한다.
		//
		
		function Encryption($str)
		{
			$sql = "select password('$str') as enc";
			$this->QueryFetch($sql);
			return $this->row->enc;
		}
		
		function QueryCount($sql)
		{
			$result = mysql_query($sql);
			$row = mysql_fetch_array($result);
		
			return $row[0];
		}
	
	}
?>