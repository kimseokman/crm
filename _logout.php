<?
	@session_start();

	//세션 비우기
	session_destroy();

	//현재 세션 모든 변수 삭제
	$_SESSION = array();

	//메인 페이지 이동
	//header("Location : index.html");
	echo "<script>document.location = 'index.html';</script>";
?>