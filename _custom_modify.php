<?
	@session_start();
?>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8"/>
<?
	include dirname(__FILE__)."/class/CAgent.php";

	$ag_han = new CAgent();
	$msgTag = "";
	$msgLevel = "";
	$msgIcon = "";

	//form validation
	if($_POST['corp_n'] == ""){		//회사명
		$msgTag = "회사 명을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['srvc_n'] == ""){	//서비스명
		$msgTag = "서비스명을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['regi_num'] == ""){	//사업자 등록번호
		$msgTag = "사업자 등록번호를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['mng_num'] == ""){	//관리번호
		$msgTag = "관리번호를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['connect_url'] == ""){	//접속 URL
		$msgTag = "고객 접속 URL을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if(($_POST['PC_cnt'] == "") && ($_POST['M_cnt'] == "")){	//상담원 ID 수
		$msgTag = "상담원 ID 수를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['a_n'] == ""){	//신청자 이름
		$msgTag = "신청 담당자 이름을 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else if($_POST['a_e'] == ""){	//멜 주소
		$msgTag = "신청 담당자 e-mail.를 입력해주세요.";
		$msgLevel = "warning";
		$msgIcon = "warning-sign";
	}else{
		/**
		 * 데이터 setting
		 */

		/**
		 * 고객 구분 setting
		 * radio
		 */

		 //고객 구분 == 해지고객
		if($_POST['c_case'] == "2"){
			$case_date = $_POST['ci_cancel_date'];	//해지일 입력
			$case_reason = $_POST['c_case_2'];		//해지 사유 종류
		}else{
			$case_date = "";						//아닐때 빈칸
			$case_reason = "";
		}

		//해지 사유 == 기타
		if($_POST['c_case_2'] == 15){
			$case_reason_etc = $_POST['stop_reason_etc'];
		}else{
			$case_reason_etc = "";
		}

		/**
		 * 기존 사용 제품 setting
		 * checkbox
		 */
		$using_prod = 0;
		if($_POST['remotecall']== "on")	$using_prod += 1;	//리모트콜
		if($_POST['aranta']== "on")		$using_prod += 2;	//아란타
		if($_POST['easyhelp']== "on")	$using_prod += 4;	//이지헬프
		if($_POST['anyhelp']== "on")	$using_prod += 8;	//애니헬프
		if($_POST['idoctor']== "on")	$using_prod += 16;	//아이닥터
		if($_POST['helpcom']== "on")	$using_prod += 32;	//헬프컴
		if($_POST['teamviewer']== "on")	$using_prod += 64;	//팀뷰어
		if($_POST['newintro']== "on")	$using_prod += 128;	//신규도입
		if($_POST['etc']== "on"){							//기타
			$using_prod += 256;
			$etc_txt = $_POST['etc_txt'];					//기타 선택시 기타 제품명 입력
		}else{
			$etc_txt = "";									//기타 입력 없으면 빈칸
		}

		/**
		 * 고객 정보 setting
		 * radio
		 */
		if($_POST['c_info'] == "in")	$c_info=1;
		else							$c_info=0;

		/**
		 * 영업 성격 setting
		 * radio
		 */
		if($_POST['b_char'] == "in")	$b_char=1;
		else							$b_char=0;

		/**
		 * 예비서버 제공 setting
		 * checkbox
		 */
		$pre_server = 0;
		if($_POST['pre_server_28'] == "on")	$pre_server += 1;	//28번 서버
		if($_POST['pre_server_35'] == "on")	$pre_server += 2;	//35번 서버
		if($_POST['pre_server_55'] == "on")	$pre_server += 4;	//55번 서버
		if($_POST['pre_server_47'] == "on")	$pre_server += 8;	//47번 서버

		/**
		 * custom_info table data update
		 * database : crm
		 */
		$ci_sql = 
		"UPDATE Custom_info SET 
		ci_case ='".$_POST['c_case']."', 
		ci_case_cancel_date ='".$case_date."',
		ci_case_cancel_case ='".$case_reason."',
		ci_case_cancel_case_etc ='".$case_reason_etc."',
		ci_using_prod='".$using_prod."', 
		ci_etc_prod='".$etc_txt."', 
		ci_info='".$c_info."', 
		ci_bsns='".$b_char."', 
		ci_pre_server='".$pre_server."',
		ci_corp_name='".$_POST['corp_n']."', 
		ci_serv_name='".$_POST['srvc_n']."', 
		ci_receller='".$_POST['recell']."', 
		ci_tax_bill_publish='".$_POST['tax_bill']."', 
		ci_home_page='".$_POST['home_p']."', 
		ci_intro_date='".$_POST['first_intro']."', 
		ci_corp_regi_num='".$_POST['regi_num']."', 
		ci_manage_num='".$_POST['mng_num']."', 
		ci_pc_ag_cnt='".$_POST['PC_cnt']."', 
		ci_mb_ag_cnt='".$_POST['M_cnt']."', 
		ci_connect_url='".$_POST['connect_url']."', 
		ci_bsns_type='".$_POST['b_type']."', 
		ci_regi_send_addr='".$_POST['regi_addr']."', 
		ci_report='".$_POST['report']."', 
		ci_document='".$_POST['docu']."', 
		ci_pay_content='".$_POST['pay_content']."', 
		ci_note='".$_POST['c_note']."'
		WHERE ci_num = '".$_POST['ci_num']."'";

		if($ag_han->CreateCustom($ci_sql)=="OK"){	//custom_info 수정 후에
			//신청 담당자 저장
			$pi_a_sql = "UPDATE Person_info SET pi_name='".$_POST['a_n']."', pi_position='".$_POST['a_p']."', pi_depart='".$_POST['a_d']."', pi_tel1='".$_POST['a_t1']."', pi_tel2='".$_POST['a_t2']."', pi_email='".$_POST['a_e']."' WHERE pi_type='0' AND ci_num='".$_POST['ci_num']."'";

			$sql_p_re = $ag_han->CreateCustom($pi_a_sql);

			//결제 담당자 저장
			$pi_p_sql = "UPDATE Person_info SET pi_name='".$_POST['p_n']."', pi_position='".$_POST['p_p']."', pi_depart='".$_POST['p_d']."', pi_tel1='".$_POST['p_t1']."', pi_tel2='".$_POST['p_t2']."', pi_email='".$_POST['p_e']."' WHERE pi_type='1' AND ci_num='".$_POST['ci_num']."'";

			$sql_p_re = $ag_han->CreateCustom($pi_p_sql);

			//영업 담당자 저장
			$pi_r_sql = "UPDATE Person_info SET pi_name='".$_POST['r_n']."', pi_position='".$_POST['r_p']."', pi_depart='".$_POST['r_d']."', pi_tel1='".$_POST['r_t1']."', pi_tel2='".$_POST['r_t2']."', pi_email='".$_POST['r_e']."' WHERE pi_type='2' AND ci_num='".$_POST['ci_num']."'";

			$sql_p_re = $ag_han->CreateCustom($pi_r_sql);

			if($sql_p_re == "OK"){
				$msgTag = "고객 정보가 수정되었습니다.";
				$msgLevel = "success";
				$msgIcon = "ok-circle";
			}
		}
	}
?>
<body>
<form method='post' id='tempForm' action='custom_modify.html'>
	<input type='hidden' name='customNum' value='<? echo $_POST['ci_num']; ?>' />
	<input type="hidden" name="alertMsg" value='<?echo $msgTag;?>' />	
	<input type="hidden" name="alertLv" value='<?echo $msgLevel;?>' />	
	<input type="hidden" name="alertIcon" value='<?echo $msgIcon;?>' />
</form>
</body>
</html>
<?
	//echo "넘어오는 값:".$_POST['bh_num'];
	echo "<script>document.getElementById('tempForm').submit();</script>";
?>