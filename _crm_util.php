<?
	function print_renew($renew_flag){
		if($renew_flag == '0'){			//미 갱신
			return '';
		}else if($renew_flag == '1'){	//갱신 완료
			return 'success';
		}else{
			return '';					//입력값 없음
		}
	}

	function current_page($input_page, $current_page){
		if($input_page == $current_page){
			return 'active';
		}else{
			return '';
		}
	}
	
	function PrintSelect($compVal, $inputVal){
		if($compVal == $inputVal){
			return "selected";
		}else{
			return "";
		}	
	}

	function PrintRadio($compVal, $inputVal){
		if($compVal == $inputVal){
			return "checked";
		}else{
			return "";
		}
	}

	function PrintRadioLabel($compVal, $inputVal){
		if($compVal == $inputVal){
			return "active";
		}else{
			return "";
		}
	}

	function PrintCheckBox($compVal, $inputVal){
		if($compVal & $inputVal){
			return "checked";
		}else{
			return "";
		}
	}

	function PrintCheckBoxLabel($compVal, $inputVal){
		if($compVal & $inputVal){
			return "active";
		}else{
			return "";
		}
	}

	function PrintPersonName($inputName){
		if($inputName=="정준모"){
			return "정준모";
		}else if($inputName=="최은진"){
			return "최은진";
		}else if($inputName=="양정용"){
			return "양정용";
		}else if($inputName=="원정일"){
			return "원정일";
		}else if($inputName=="오창건"){
			return "오창건";
		}else if($inputName=="정현준"){
			return "정현준";
		}else if($inputName=="서성원"){
			return "서성원";
		}else{
			return "없음";
		}
	}

	function PrintRefer($inputRefer){
		if($inputRefer=="최초도입"){
			return "최초도입";
		}else if($inputRefer=="갱신"){
			return "갱신";
		}else if($inputRefer=="추가"){
			return "추가";
		}else if($inputRefer=="사용중지"){
			return "사용중지";
		}else if($inputRefer=="M"){
			return "M";
		}else{
			return "없음";
		}
	}

	function PrintDate($inputDate){
		if($inputDate != "0000-00-00"){
			return $inputDate;
		}else{
			return "";
		}
	}
?>