<?
	@session_start();
?>
<meta http-equiv="Content-Type"  content="text/html; charset=UTF-8"/>
<?
	include dirname(__FILE__)."/class/CUser.php";

	$log_han = new CUser();
	$msgTag = "";
	$msgLevel = "";
	$msgIcon = "";

	$log_re = $log_han->Login($_POST['userID'], $_POST['userPW']);

	if($log_re == "MANAGER" || $log_re == "ADMIN"){	//로그인
		$user = $log_han->getUserInfo($_POST['userID']);

		$_SESSION['userID'] = $user->ui_id;
		$_SESSION['userAuth'] = $user->ui_auth;
		//echo $user->ui_isAdmin;

		if($user->ui_isAdmin)	$_SESSION['idAdmin'] = "ADMIN";
		else					$_SESSION['idAdmin'] = "MANAGER";
		
		$msgTag = ":: ".$_SESSION['idAdmin']." Account :: 로그인 성공!";
		$msgLevel = "success";
		$msgIcon = "ok-circle";
		
		$_SESSION['isLogin'] = true;
		
		echo "<script> document.location.href = 'index.html'; </script>";
		
	}else{	//로그인 실패
		if($log_re == "NOID" || $log_re == "NOPASSWORD"){	//경고
			switch($log_re){
				case "NOID" : $msgTag = "Please enter your ID."; break;
				case "NOPASSWORD" : $msgTag = "Please enter your PW."; break;
			}
			$msgLevel = "warning";
			$msgIcon = "warning-sign";
		}else{	//error
			$msgTag = "ID와 PW를 확인해주세요.";
			$msgLevel = "danger";
			$msgIcon = "ban-circle";
		}
		
		$_SESSION['isLogin'] = false;
?>
	<form method="post" id="loginAlertMsgForm" action="login.html">
		<input type="hidden" name="alertMsg" value='<?echo $msgTag;?>' />	
		<input type="hidden" name="alertLv" value='<?echo $msgLevel;?>' />
		<input type="hidden" name="alertIcon" value='<?echo $msgIcon;?>' />	
	</form>
<?
		echo "<script>document.getElementById('loginAlertMsgForm').submit();</script>";
	}
?>