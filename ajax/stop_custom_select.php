<?
function PrintDate($inputDate){
	if($inputDate != "0000-00-00"){
		return $inputDate;
	}else{
		return "";
	}
}

function PrintSelect($compVal, $inputVal){
	if($compVal == $inputVal){
		return "selected";
	}else{
		return "";
	}	
}

//ajax 값 안들어 올 경우 ajax 값 걸러내기
if($_POST['ci_cancel_date'] == null) $_POST['ci_cancel_date'] = "";
if($_POST['c_case_2'] == null) $_POST['c_case_2'] = "";
if($_POST['cancel_etc'] == null) $_POST['cancel_etc'] = "";

if($_POST['c_case'] == "true"){	//해지 고객일 경우
?>
<div class="regi_line">
	<p>
		<label class="control-label regi_label" for="ci_cancel_date">해지일</label>
		<p class="regi_input2">
			<input class="form-control input-sm" type="text" title="해지일" class="ci_cancel_date" id="ci_cancel_date" name="ci_cancel_date" value="<? echo PrintDate($_POST['ci_cancel_date']); ?>"/>
		</p>
	</p>
	<p>
		<label class="control-label regi_label" for="c_case_2">해지 사유</label>
		<p class="regi_input2">
			<select class="form-control input-sm" id="c_case_2" name="c_case_2" title="해지 사유" onclick="ReqStopCustomEtc('','','<? echo $_POST['cancel_etc']; ?>');">
				<option value="0" <? echo PrintSelect(0, $_POST['c_case_2']); ?> >:: 해지 사유 선택 ::</option>
				<option value="1" <? echo PrintSelect(1, $_POST['c_case_2']); ?> >사용량 저하</option>
				<option value="2" <? echo PrintSelect(2, $_POST['c_case_2']); ?> >회사 폐업 및 인수</option>
				<option value="3" <? echo PrintSelect(3, $_POST['c_case_2']); ?> >원격 수요 없음</option>
				<option value="4" <? echo PrintSelect(4, $_POST['c_case_2']); ?> >서비스 종료</option>
				<option value="5" <? echo PrintSelect(5, $_POST['c_case_2']); ?> >예산 미편성</option>
				<option value="6" <? echo PrintSelect(6, $_POST['c_case_2']); ?> >관공서 접속 안됨</option>
				<option value="7" <? echo PrintSelect(7, $_POST['c_case_2']); ?> >제품 불만족</option>
				<option value="8" <? echo PrintSelect(8, $_POST['c_case_2']); ?> >제품변경_리모트콜</option>
				<option value="9" <? echo PrintSelect(9, $_POST['c_case_2']); ?> >제품변경_리모트콜M</option>
				<option value="10" <? echo PrintSelect(10, $_POST['c_case_2']); ?> >제품변경_헬프유</option>
				<option value="11" <? echo PrintSelect(11, $_POST['c_case_2']); ?> >제품변경_이지헬프</option>
				<option value="12" <? echo PrintSelect(12, $_POST['c_case_2']); ?> >제품변경_씨트롤</option>
				<option value="13" <? echo PrintSelect(13, $_POST['c_case_2']); ?> >제품변경_피씨애니프로</option>
				<option value="14" <? echo PrintSelect(14, $_POST['c_case_2']); ?> >제품변경_웹엑스</option>
				<option value="15" <? echo PrintSelect(15, $_POST['c_case_2']); ?> >기타</option>
			</select>
		</p>
	</p>
</div>
<?
}else{	//아닐 경우 반응 X
}
?>