﻿<div class="ajax_keyword2">
<?
if($_POST['s_keyword'] == 0){					//선택X
} else if($_POST['s_keyword'] == 1){			//회사명
?>
	<div class="input-group">
		<span class="input-group-addon">이름</span>
		<input type="text" id="keyword1_1_text" placeholder="회사명 입력" class="form-control input-sm" name="keyword1_1_text" value="" title="키워드 입력" />
	</div>
<?
} else if($_POST['s_keyword'] == 2){		//고객 구분
?>
	<div class="input-group">
		<span class="input-group-addon">구분</span>
		<select class="form-control input-sm" id="keyword1_1_select" name="keyword1_1_select">
			<option value="0">:: 고객구분 선택 ::</option>
			<option value="1">유료고객</option>
			<option value="2">해지고객</option>
			<option value="3">갱신보류</option>
			<option value="4">리셀러</option>
			<option value="5">티소프트</option>
			<option value="6">CMS</option>
			<option value="7">무상제공</option>
			<option value="8">TEST제공</option>
			<option value="9">재사용</option>
			<option value="10">솔루션</option>
		</select>
	</div>
<?
} else if($_POST['s_keyword'] == 3){		//기존 사용 제품
?>
	<div class="input-group">
		<span class="input-group-addon">제품</span>
		<select class="form-control input-sm" id="keyword1_1_select" name="keyword1_1_select">
			<option value="0">:: 기존 사용 제품 선택 ::</option>
			<option value="1">리모트콜</option>
			<option value="2">아란타</option>
			<option value="4">이지헬프</option>
			<option value="8">애니헬프</option>
			<option value="16">아이닥터</option>
			<option value="32">헬프컴</option>
			<option value="64">팀뷰어</option>
			<option value="128">신규도입</option>
			<option value="256">기타</option>
		</select>
	</div>
<?
} else if($_POST['s_keyword'] == 4){		//고객 정보
?>
	<label for="keyword1_1_radio_in">
		<input id="keyword1_1_radio_in" title="고객정보" type="radio" name="c_info" value="in" checked/> IN
	</label>
	<label for="keyword1_1_radio_out">
		<input id="keyword1_1_radio_out" title="고객정보" type="radio" name="c_info" value="out"/> OUT
	</label>
<?
} else if($_POST['s_keyword'] == 5){		//영업 성격
?>
	<label for="keyword1_1_radio_in">
		<input id="keyword1_1_radio_in" title="영업 성격" type="radio" name="b_char" value="in" checked/> IN
	</label>
	<label for="keyword1_1_radio_out">
		<input id="keyword1_1_radio_out" title="영업성격" type="radio" name="b_char" /> OUT
	</label>
<?
} else if($_POST['s_keyword'] == 6){		//서비스 명
?>
	<div class="input-group">
		<span class="input-group-addon">이름</span>
		<input type="text" id="keyword1_1_text" placeholder="서비스명 입력" class="form-control input-sm" name="keyword1_1_text" value="" title="키워드 입력" />
	</div>
<?
} else if($_POST['s_keyword'] == 7){		//리셀러
?>
	<div class="input-group">
		<span class="input-group-addon">리셀러</span>
		<input type="text" id="keyword1_1_text" placeholder="리셀러 입력" class="form-control input-sm" name="keyword1_1_text" value="" title="키워드 입력" />
	</div>
<?
} else if($_POST['s_keyword'] == 8){		//세/계 발행처
?>
	<div class="input-group">
		<span class="input-group-addon">발행처</span>
		<input type="text" id="keyword1_1_text" placeholder="세/계 발행처 입력" class="form-control input-sm" name="keyword1_1_text" value="" title="키워드 입력" />
	</div>	
<?
} else if($_POST['s_keyword'] == 9){		//홈페이지
?>
	<div class="input-group">
		<span class="input-group-addon">URL</span>
		<input type="text" id="keyword1_1_text" placeholder="URL 입력" class="form-control input-sm" name="keyword1_1_text" title="키워드 입력" value="" />
	</div>
<?
} else if($_POST['s_keyword'] == 10){		//Admin
?>
	<div class="input-group">
		<span class="input-group-addon">ID</span>
		<input type="text" id="keyword1_1_text" placeholder="Admin ID 입력" class="form-control input-sm" name="keyword1_1_text" title="키워드 입력" value="" />
	</div>
<?
} else if($_POST['s_keyword'] == 11){		//영업자
?>
	<div class="input-group">
		<span class="input-group-addon">이름</span>
		<select class="form-control input-sm" id="keyword1_1_select" name="keyword1_1_select">
			<option value="0">:: 영업자 선택 ::</option>
			<option value="정준모">정준모</option>
			<option value="최은진">최은진</option>
			<option value="양정용">양정용</option>
			<option value="원정일">원정일</option>
			<option value="오창건">오창건</option>
			<option value="정현준">정현준</option>
			<option value="서성원">서성원</option>
		</select>
	</div>
<?
} else if($_POST['s_keyword'] == 12){		//사업자 등록번호
?>
	<div class="input-group">
		<span class="input-group-addon">등록번호</span>
		<input type="text" id="keyword1_1_text" placeholder="사업자 등록번호 입력" class="form-control input-sm" name="s_key_txt" title="키워드 입력" value="" />
	</div>
<?
} else if($_POST['s_keyword'] == 13){		//최초 도입일
?>
	<div class="input-group">
		<span class="input-group-addon">도입일</span>
		<input type="text" id="keyword1_1_s_date" class="form-control input-sm" name="keyword1_1_s_date" title="시작일" /> 
		<span class="input-group-addon">부터</span>
		<input type="text" id="keyword1_1_e_date" class="form-control input-sm" name="keyword1_1_e_date" title="만료일" />
		<span class="input-group-addon">까지</span>
	</div>
<?
} else if($_POST['s_keyword'] == 14){		//만료일
?>
	<div class="input-group">
		<span class="input-group-addon">만료일</span>
		<input type="text" id="keyword1_1_s_date" class="form-control input-sm" name="keyword1_1_s_date" title="시작일" /> 
		<span class="input-group-addon">부터</span>
		<input type="text" id="keyword1_1_e_date" class="form-control input-sm" name="keyword1_1_e_date" title="만료일" />
		<span class="input-group-addon">까지</span>
	</div>
<?
} else if($_POST['s_keyword'] == 15){		//관리번호
?>
	<div class="input-group">
		<span class="input-group-addon">번호</span>
		<input class="form-control input-sm" type="text" placeholder="관리번호 입력" id="keyword1_1_text" name="keyword1_1_text" />
	</div>
<?
} else if($_POST['s_keyword'] == 16){		//M사용고객

//ajax 없음

} else if($_POST['s_keyword'] == 17){		//업종
?>
	<div class="input-group">
		<span class="input-group-addon">업종</span>
		<select class="form-control input-sm" id="keyword1_1_select" name="keyword1_1_select">
			<option value="0">:: 업종 선택 ::</option>
			<option value="1">대학교</option>				
			<option value="2">평생교육원</option>
			<option value="3">유지보수</option>
			<option value="4">온라인교육</option>
			<option value="5">IT서비스</option>
			<option value="6">ERP솔루션</option>
			<option value="7">PC수리복구</option>
			<option value="8">POS</option>
			<option value="9">PC유지보수</option>
			<option value="10">게임</option>
			<option value="11">쇼핑몰</option>
			<option value="12">교육원</option>
			<option value="13">관공서</option>
			<option value="14">교회</option>
			<option value="15">병원</option>
			<option value="16">디자인</option>
			<option value="17">로봇</option>
			<option value="18">호스팅</option>
			<option value="19">모바일서비스</option>
			<option value="20">방송,통신</option>
			<option value="21">백신SW</option>
			<option value="22">보안솔루션</option>
			<option value="23">부동산</option>
			<option value="24">세무회계</option>
			<option value="25">소프트웨어개발</option>
			<option value="26">여행사</option>
			<option value="27">CAD</option>
			<option value="28">파일공유</option>
			<option value="29">의료기기</option>
			<option value="30">입찰정보</option>
			<option value="31">대출</option>
			<option value="32">전자세금계산서</option>
			<option value="33">주식정보</option>
			<option value="34">퀵서비스</option>
		</select>
	</div>
<?
} else if($_POST['s_keyword'] == 18){		//상담원ID
?>
	<div class="input-group">
		<span class="input-group-addon">ID</span>
		<input class="form-control input-sm" type="text" placeholder="상담원 ID 입력" id="keyword1_1_text" name="keyword1_1_text" />
	</div>
<?
} else if($_POST['s_keyword'] == 19){		//해지일
?>
	<div class="input-group">
		<span class="input-group-addon">해지일</span>
		<input type="text" id="keyword1_1_s_date" class="form-control input-sm" name="keyword1_1_s_date" title="시작일" /> 
		<span class="input-group-addon">부터</span>
		<input type="text" id="keyword1_1_e_date" class="form-control input-sm" name="keyword1_1_e_date" title="만료일" />
		<span class="input-group-addon">까지</span>
	</div>
<?
} else {								//기타

}
?>
</div>




