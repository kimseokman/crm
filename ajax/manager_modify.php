<!-- modify_manager -->
<div class="modify_manager">
<?

	include dirname(__FILE__)."/../class/CAdmin.php";

	$admin_han = new CAdmin();

	$sql = "select * from user_info where ui_num='".$_POST['agentNum']."'";

	$select_agent = $admin_han->GetAgent($sql);

?>
	<!-- panel -->
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">매니저 수정</h3>
		</div>
		<!-- //panel_head -->
		<div class="panel-body">
			<form method="post" class="form-horizontal" id="agentInfo" role="form" name="agentInfo">
			<fieldset>
				<legend class="sr-only">매니저 수정</legend>
				<div class="form-group">
					<label title="번호컬럼" class="col-sm-offset-2 col-sm-2 control-label">No</label>
					<div class="col-sm-4">
						<p class="form-control-static">
							<?= $select_agent->ui_num; ?>
						</p>
					</div>
					<input type="hidden" name="agentNum" value=<?= $select_agent->ui_num; ?> />
				</div>
				<div class="form-group">
					<label title="매니저 아이디" class="col-sm-offset-2 col-sm-2 control-label">ID</label>
					<div class="col-sm-4">
						<p class="form-control-static">
							<span class="text-primary">
								<?= $select_agent->ui_id; ?>
							</span>
						</p>
					</div>
				</div>
				<div class="form-group">
					<label title="매니저 패스워드" class="col-sm-offset-2 col-sm-2 control-label">PW</label>
					<div class="col-sm-4">
						<input type="password" class="form-control input-sm" id="AgentPW" name="AgentPW" value="" />
					</div>
				</div>
				<div class="form-group">
					<label title="매니저 패스워드 확인" class="col-sm-offset-2 col-sm-2 control-label">PW확인</label>
					<div class="col-sm-4">
						<input type="password" class="form-control input-sm" id="AgentPW2" name="AgentPW2" value="" />
					</div>
				</div>
				<?
					$auth_s_chk="";
					$auth_s_chk_label = "";
					$auth_m_chk="";
					$auth_m_chk_label="";
					$auth_d_chk="";
					$auth_d_chk_label="";

					if($select_agent->ui_auth & 1){
						$auth_s_chk = "checked";
						$auth_s_chk_label = "active";
					}
					if($select_agent->ui_auth & 2){
						$auth_m_chk = "checked";
						$auth_m_chk_label = "active";
					}
					if($select_agent->ui_auth & 4){
						$auth_d_chk = "checked";
						$auth_d_chk_label = "active";
					}
				?>
				<div class="form-group">
					<label class="col-sm-offset-2 col-sm-2 control-label" title="매니저 권한">Auth</label>
					<div class="col-sm-3">
						<div class="btn-group">
							<label title="엑셀저장" for="auth_excel" class="">
								<input name="auth_excel" id="auth_excel" type="checkbox" <? echo $auth_s_chk; ?> /> 엑셀저장
							</label>
							<label title="수정" for="auth_modify" class="">
								<input name="auth_modify" id="auth_modify" type="checkbox" <? echo $auth_m_chk; ?> /> 수정
							</label>
							<label title="다운로드" for="auth_down" class="">
								<input name="auth_down" id="auth_down" type="checkbox" <? echo $auth_d_chk; ?> /> 다운로드
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-4">
						<div class="btn-group">
							<button type="submit" class="btn btn-primary modify_btn" title="매니저 수정" onclick="this.form.action = '_manager_modify.php';" >
								Modify
							</button>
							<button type="submit" class="btn btn-danger delete_btn" title="매니저 삭제" onclick="this.form.action='_manager_delete.php';" >
								Delete
							</button>
						</div>
					</div>
				</div>
			</fieldset>
			</form>
		</div>
		<!-- //panel_body -->
	</div>
	<!-- //panel -->
</div>
<!-- //modify_manager -->